---
layout: page
title: About
permalink: /about/
---

Hello and welcome! I'm Matt Young, a seasoned Data Security Analyst with a rich background in IT and cybersecurity, rooted deeply within the public sector. My journey spans over a decade, marked by relentless dedication to vulnerability management, risk assessments, and the development of comprehensive security strategies. Currently, I'm proud to serve the State of Colorado, where my expertise in leading penetration testing initiatives and malware analysis plays a pivotal role in safeguarding sensitive information and fostering a culture of cybersecurity awareness.

My professional path is underpinned by a strong educational foundation, with an Associate's Degree in Applied Science (Cyber Security) from Red Rocks Community College. This academic pursuit was complemented by my active engagement in cybersecurity extracurriculars, where I demonstrated leadership and technical proficiency as both a Penetration Testing Leader and Red Team Leader.

Beyond the confines of conventional work, my passion for cybersecurity takes on a creative form through this website. Here, I share my experiences, challenges, and the innovative solutions I've encountered in my career. My aim is to offer insights into the captivating world of CTF challenges and ethical hacking, serving both as a portfolio of my work and a resource for those embarking on their cybersecurity journey.

My certifications, including the EC-Council's CEH (Certified Ethical Hacker), CompTIA Security+, and CHP (Certified HIPAA Professional), are testaments to my commitment to excellence and continuous learning in this ever-evolving field. These credentials, alongside my hands-on experience with leading cybersecurity tools and frameworks, empower me to craft robust security measures and strategies tailored to the nuanced needs of the public sector.

At the heart of my approach to cybersecurity is a profound respect for the delicate balance between technological innovation and the imperatives of privacy and security. Whether through leading sophisticated phishing campaign simulations, enhancing organizational security postures, or developing security awareness programs, my focus remains steadfast on elevating cybersecurity awareness and resilience.

This site is a reflection of my professional journey and a platform to connect with like-minded individuals and organizations. Whether you're a fellow cybersecurity professional, an aspiring hacker, or a potential employer, I invite you to explore my work and share in the dialogue of securing our digital world.

Feel free to reach out to me if you have any inquiries. 
